package dummy;

public class EvaluateFormula {

	public String[] evaluate(String text) {
		String ok = "<span style=\"color: #339966;\">No errors in formula</span>";
		String noText = "<span style=\"color: #ff0000;\">Nothing to evaluate!</span>";
		String fullStack ="<span style=\"color: #ff0000;\">Dummy Stack Exception:<br />Line 13 in file xxx<br />Line 15 in file yyy</span>";
		String error = "<span style=\"color: #ff0000;\">Error in Line xxx:<br />Click <a href=\"blank\">here</a> to show full stack.</span>";
		String[] result;
		switch (text) {
		case "":
			result = new String[] {noText};
			break;
		case "Error":
			result = new String[] {error, fullStack};
			break;
		default:
			result = new String[] {ok};
		}
		return result;
	}

}
