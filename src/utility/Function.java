package utility;
import java.util.ArrayList;
import javafx.util.Pair;


public class Function {
	public String name;
	public String returnType = null;
	public ArrayList<Pair<String, String>> pars = new ArrayList<Pair<String, String>>();
	public String documentation;
	
	public Function() {}
	public Function(String nameAndRT, String Parameters, String Doc) {
		documentation = Doc;
		String[] nameAndRTSep = nameAndRT.split(" ");
		if (nameAndRTSep.length>1) {
			name = nameAndRTSep[1];
			returnType = nameAndRTSep[0];
		} else {
			name = nameAndRTSep[0];
		}
		
		String[] parSep = Parameters.split(", ");
		for (String par : parSep) {
			String[] divided = par.split(" ");
			if (divided.length>1) pars.add(new Pair<String, String>(divided[0], divided[1]));
			else pars.add(new Pair<String, String>("Unknown", divided[0]));
		}
		
	}
}
