package utility;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {

public ArrayList<Function> createGroups() {
	
		String file = readFile("C:\\Users\\erald\\AppData\\Roaming\\Freeplane\\1.7.x\\lib\\Custom.groovy");
		ArrayList<Function> result = new ArrayList<Function>();
		Pattern pat = Pattern.compile("def static (.*)\\((.*)\\)");
		Matcher matcher = pat.matcher(file);
		while (matcher.find()) {
			Function g = new Function(matcher.group(1), matcher.group(2), "Doc");
			result.add(g);
		}
		return result;
	}

public String readFile(String fileName) {
	String fileString = "";
	try {
		fileString = new String(Files.readAllBytes(Paths.get(fileName)));
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println("No Custom File");
	}
	return fileString;
}
}