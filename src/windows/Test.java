package windows;

import java.awt.EventQueue;
import logic.*;
import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.w3c.dom.Document;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import org.freeplane.api.Node;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ScrollPaneConstants;

public class Test {
	private RTextScrollPane builderContainerPanel;
	private JEditorPane EvaluationPanel;
	private JFrame frmFormulaBuilder;
	private TextAreas rTest;
	private String[] result;
	private FreePlane fp;
	JTree tree = new JTree();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Test window = new Test(args[0]);
					window.frmFormulaBuilder.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @param path 
	 */
	public Test(String path) {
		fp = new FreePlane();
		initialize(path);
		
	}

	private void Evaluate() {
		EvaluationPanel.setEnabled(true);
		dummy.EvaluateFormula Evaluator = new dummy.EvaluateFormula();
		 result = Evaluator.evaluate(builderContainerPanel.getTextArea().getText());
		EvaluationPanel.setText(result[0]);
	}

	private void sendToFreeplan() {
		System.out.print(builderContainerPanel.getTextArea().getText());
		frmFormulaBuilder.dispose();
	}

	/**
	 * Initialize the contents of the frame.
	 * @param detach 
	 */
	private void initialize(String path) {
		File templatesXML = new File(System.getProperty("user.dir") + "/resources/templates.xml");
		File methodsXML = new File(System.getProperty("user.dir") + "/resources/methods.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document docTemplates = null;
		Document docMethods = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			docTemplates = dBuilder.parse(templatesXML);
			docTemplates.getDocumentElement().normalize();
			
            docMethods = dBuilder.parse(methodsXML);
            docMethods.getDocumentElement().normalize();
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		File installDir = new File("C:\\Program Files\\Freeplane");
		System.out.println(path);
		File fileToOpen = new File(path);
		frmFormulaBuilder = new JFrame();
		frmFormulaBuilder.setTitle("Formula Builder");
		frmFormulaBuilder.setBounds(100, 100, 800,600);
		frmFormulaBuilder.getDefaultCloseOperation();
		frmFormulaBuilder.getContentPane().setLayout(new MigLayout("", "[30.61%:34.57%:33.04%,grow][grow]", "[:37.00:5.17%,grow][grow][][grow,fill][]"));

		rTest = new TextAreas();
		builderContainerPanel = rTest.getTextWindow(docTemplates, docMethods);
		builderContainerPanel.getTextArea().append("=");
		
		tree = fp.returnTree(installDir, fileToOpen);
    	tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    	TreePath pathToSelectedNode = fp.find(tree);
		tree.setSelectionPath(pathToSelectedNode);
   		tree.scrollPathToVisible(pathToSelectedNode);
		JCheckBox chckbxTemplates = new JCheckBox("Suggest Templates");
		chckbxTemplates.setHorizontalAlignment(SwingConstants.RIGHT);
		chckbxTemplates.setSelected(true);
		chckbxTemplates.addItemListener(new ItemListener() {
			@Override
		    public void itemStateChanged(ItemEvent e) {
		        if(e.getStateChange() == ItemEvent.SELECTED) {
		        	rTest.filterFunctionsOut(100, true);
		        } else {//checkbox has been deselected
		        	rTest.filterFunctionsOut(100, false);
		        };
		    }
		});
		
		JButton btnEvaluate = new JButton("Evaluate Formula");

		ButtonGroup pathSelector = new ButtonGroup();

		
		JScrollPane navigationTab = new JScrollPane();
		frmFormulaBuilder.getContentPane().add(navigationTab, "cell 0 0 1 2,grow");
		navigationTab.setViewportView(tree);
		JPanel radioPanel = new JPanel();
		radioPanel.setAlignmentY(0.0f);
		frmFormulaBuilder.getContentPane().add(radioPanel, "cell 1 0,grow");
		JRadioButton btnSetAbsolute = new JRadioButton("Use Absolute Path");
		btnSetAbsolute.setSelected(true);
		radioPanel.add(btnSetAbsolute);
		pathSelector.add(btnSetAbsolute);
		
		JRadioButton btnSetRelative = new JRadioButton("Use Relative Path");
		radioPanel.add(btnSetRelative);
		pathSelector.add(btnSetRelative);
		
		JRadioButton rdbtnUseId = new JRadioButton("Use Id");
		radioPanel.add(rdbtnUseId);
		pathSelector.add(rdbtnUseId);
		
		
		JScrollPane RTextContainer = new JScrollPane();
		RTextContainer.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		RTextContainer.setViewportView(builderContainerPanel);
		frmFormulaBuilder.getContentPane().add(RTextContainer, "cell 1 1,grow");
		frmFormulaBuilder.getContentPane().add(btnEvaluate, "cell 0 2");
		
		JCheckBox chckbxSuggestCustomFunctions = new JCheckBox("Suggest Custom Functions");
		chckbxSuggestCustomFunctions.setSelected(true);
		chckbxSuggestCustomFunctions.addItemListener(new ItemListener() {
			@Override
		    public void itemStateChanged(ItemEvent e) {
		        if(e.getStateChange() == ItemEvent.SELECTED) {
		        	rTest.showFunctions(true);
		        } else {//checkbox has been deselected
		        	rTest.showFunctions(false);
		        };
		    }
		});
		frmFormulaBuilder.getContentPane().add(chckbxSuggestCustomFunctions, "flowx,cell 1 2,alignx right");
		frmFormulaBuilder.getContentPane().add(chckbxTemplates, "cell 1 2,push ,alignx right");
		JCheckBox chckbxFormulas = new JCheckBox("Suggest Functions");
		chckbxFormulas.setSelected(true);
		chckbxFormulas.addItemListener(new ItemListener() {
			@Override
		    public void itemStateChanged(ItemEvent e) {
		        if(e.getStateChange() == ItemEvent.SELECTED) {
		        	rTest.filterFunctionsOut(90, true);
		        } else {//checkbox has been deselected
		        	rTest.filterFunctionsOut(90, false);
		        };
		    }
		});
		frmFormulaBuilder.getContentPane().add(chckbxFormulas, "cell 1 2");
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setMinimumSize(new Dimension(23, 150));
		frmFormulaBuilder.getContentPane().add(scrollPane, "cell 0 3 2 1,grow");
		
		EvaluationPanel = new JEditorPane();
		scrollPane.setViewportView(EvaluationPanel);
		EvaluationPanel.setBorder(null);
		EvaluationPanel.setEditable(false);
		EvaluationPanel.setContentType("text/html");
		EvaluationPanel.setToolTipText("");
		EvaluationPanel.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))
                    	EvaluationPanel.setText(result[1]);
            }

				
		});
		
		JButton btnOk = new JButton("Save");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sendToFreeplan();
			}


		});
		frmFormulaBuilder.getContentPane().add(btnOk, "cell 1 4,push ,alignx right");
		//frame.getContentPane().add(jp, "cell 1 0,grow");
		
		
/*	    tree.addMouseListener(new MouseAdapter() {
	        public void mouseClicked(MouseEvent me) {
	          doMouseClicked(me);
	        }
	      });*/
	    
	    MouseListener ml = new MouseAdapter() {
		    public void mousePressed(MouseEvent e) {
		        int selRow = tree.getRowForLocation(e.getX(), e.getY());
		        TreePath selPath = tree.getPathForLocation(e.getX(), e.getY());
		        if(selRow != -1) 
		        	if(e.getClickCount() == 2) 
		            	writeNodeOrAttribute(selPath);
		    }

			private void writeNodeOrAttribute(TreePath selPath) {
				String result = "";
				Boolean absolutePath = btnSetAbsolute.isSelected();
				Boolean useId = rdbtnUseId.isSelected();
				DefaultMutableTreeNode clicked = (DefaultMutableTreeNode) selPath.getLastPathComponent();
				DefaultMutableTreeNode runner = clicked;
				String path = "";
				
				//Create Path
				if (selPath.getParentPath() != null && clicked.getUserObject().getClass().equals(logic.Attribute.class)) {
					runner = (DefaultMutableTreeNode) clicked.getParent(); }
					Node clickedNode = ((MapNode) runner.getUserObject()).mapNode;
					System.out.println("Clicked on the node: " + clickedNode.getId());
        			if (useId) {
        				result = clickedNode.getId();
        				path = result;
					} else {
					path = fp.createFreePlanePath(clickedNode, absolutePath);
            		result = path;
					}
				//Add attribute to Path
				if (selPath.getParentPath() != null && clicked.getUserObject().getClass().equals(logic.Attribute.class)) 
					result = path + "['" + selPath.getLastPathComponent() + "']";
            	builderContainerPanel.getTextArea().replaceSelection("");
            	builderContainerPanel.getTextArea().insert(result, builderContainerPanel.getTextArea().getCaretPosition());
			}
	    };
	    tree.addMouseListener(ml);
		btnEvaluate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Evaluate();
			}
		});
	}
	

}


