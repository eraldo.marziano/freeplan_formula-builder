package logic;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.text.JTextComponent;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ParameterizedCompletion.Parameter;

import javafx.util.Pair;
import utility.Function;
import utility.Parser;
import utility.customFunctionCompletion;

public class FunctionsCompletionProvider extends DefaultCompletionProvider{

	public FunctionsCompletionProvider() {
		super();
	}
	public FunctionsCompletionProvider(String[] words) {
		super(words);
	}

	@Override
	protected List<Completion> getCompletionsImpl(JTextComponent comp) {
		List<Completion> complets = new ArrayList<Completion>();
		List<Completion> temp = super.getCompletionsImpl(comp);
		for (Completion completion : temp) {
			complets.add(completion);
		}
		List<Completion> varToAdd = getCustomCompletions(comp);
			if (varToAdd!=null) {
			complets.addAll(varToAdd);
		}
		return complets;
	}

	private ArrayList<Completion> getCustomCompletions(JTextComponent comp) {
		ArrayList<Completion> varCompletions = new ArrayList<Completion>();
		CompletionProvider p = new DefaultCompletionProvider();
		String text = p.getAlreadyEnteredText(comp);
		System.out.println(text);
		if (text.equals("custom")) {
			ImageIcon ico = new ImageIcon("./resources/Template.png");
			Parser pa = new Parser();
			ArrayList<Function> functions = pa.createGroups();
			for (Function function : functions) {
				String rT = (function.returnType == null) ? "Undefined" : function.returnType;
				customFunctionCompletion fc = new customFunctionCompletion(this, function.name, rT);
				fc.setIcon(ico);
				fc.setRelevance(170);
				List<Parameter> params = new ArrayList<Parameter>();
				for (Pair<String, String> par : function.pars) {
					params.add(new Parameter(par.getKey().toString(), par.getValue().toString()));
				}
				fc.setParams(params);
				fc.setShortDescription(function.documentation);
				varCompletions.add(fc); 
			}
		}
		return varCompletions; 
	}
}
