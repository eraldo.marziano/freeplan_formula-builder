package logic;
public class Attribute {
	public String name;
	public String value;
	
	
	public Attribute(String value, String name) {
		this.name = name;
		this.value = value;
	}

	@Override
	public String toString() {
		return name ;
	}
}
