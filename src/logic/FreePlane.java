package logic;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.freeplane.api.Attributes;
import org.freeplane.api.Loader;
import org.freeplane.api.MindMap;
import org.freeplane.api.Node;
import org.freeplane.api.NodeCondition;
import org.freeplane.api.NodeRO;
import org.freeplane.plugin.script.proxy.*;
import org.freeplane.plugin.script.proxy.Proxy.Controller;

public class FreePlane {
	private MindMap FreePlaneMap;
	private Controller ctrl = ScriptUtils.c();
	private DefaultMutableTreeNode treeRootNode;
	private org.freeplane.api.Node mapRootNode;
	private Node selectedNode;
	

	public FreePlane() {
		selectedNode = ctrl.getSelected();
	}
	
	public JTree returnTree(File freeplaneDirectory, final File fileToOpen) {
		Loader load = ctrl.mapLoader(fileToOpen);
    	FreePlaneMap = load.getMindMap();
    	//select the Root node of the Map
    	mapRootNode = FreePlaneMap.getRoot();
    	MapNode InternalNode = new MapNode(mapRootNode, mapRootNode.getPlainText(), mapRootNode.getId());
    	treeRootNode = new DefaultMutableTreeNode(InternalNode);
    	setAttributes(treeRootNode);
    	scrapeNodes(treeRootNode);

    	JTree tree = new JTree(treeRootNode);

    	return tree;
	}

	
	@SuppressWarnings("unchecked")
	public TreePath find(JTree tree) {
		Node selected = ctrl.getSelected();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) tree.getModel().getRoot();
		Enumeration<DefaultMutableTreeNode> e = root.depthFirstEnumeration();
		while (e.hasMoreElements()) {
			DefaultMutableTreeNode dMT = e.nextElement();
			if (dMT.getUserObject().getClass().equals(logic.MapNode.class)) {
				Node n = ((MapNode) dMT.getUserObject()).mapNode;
				if (selected.getId().equals(n.getId())) {
					DefaultTreeModel model = (DefaultTreeModel) tree.getModel();  
					TreeNode[] nodes = model.getPathToRoot(dMT);
					return new TreePath(nodes);
				}
			}
		}
		return null;
	}


	public String createFreePlanePath(Node n, boolean isAbsolute) {
		// Ideally, the freeplane API should return a valid Path for the node, in a String variable
		String result = "node.at(\"";
		if (isAbsolute) {
			System.out.println("ID: " + n.getId());
			List<? extends Node> list = n.getPathToRoot();
			result = result + "..";
			for (Node node : list) {
				System.out.println("    " + node.getPlainText());
				result = result + "'" + node.getPlainText() + "'/";
			}
			result = result.substring(0, result.length()-1);
			result = result + "\")";
		} else {
			Node commonAncestor = findAncestor(n, selectedNode);
			System.out.println("Ancestor is " + commonAncestor.getPlainText());
			Node runner = selectedNode;
			while (runner.getId() != commonAncestor.getId()) {
				result = result + "../";
				runner = runner.getParent();
			}
			List<String> descendingTexts = new ArrayList<String>();
			runner = n;
			while (runner.getId() != commonAncestor.getId()) {
				descendingTexts.add(runner.getPlainText());
				runner = runner.getParent();
			}
			Collections.reverse(descendingTexts);
			for (String string : descendingTexts) {
				result = result + "'" + string + "'/";
			}
			if (result.endsWith("/"))
				result = result.substring(0, result.length()-1);
			result = result + "\")";	
		}
		System.out.println(result);
		return result;
	}


	private Node findAncestor(Node clicked, Node selected) {
			int depthSelected = selected.getNodeLevel(true);
			int depthClicked = clicked.getNodeLevel(true);
			Node travelSelected = selected;
			Node travelClicked = clicked;
			System.out.println("Clicked: " + depthClicked + " - Selected: " + depthSelected );
			while (depthSelected != depthClicked) {
				if (depthSelected > depthClicked) {
					travelSelected = travelSelected.getParent();
					System.out.println(travelSelected.getText());
					depthSelected--;
				}
				else {
					travelClicked = travelClicked.getParent();
					System.out.println(travelClicked.getText());
					depthClicked--;
				}
			}
			while (travelClicked.getId() != travelSelected.getId()) {
				System.out.println("Same depth");
				
				travelSelected = travelSelected.getParent();
				
				travelClicked = travelClicked.getParent();
			}
		return travelSelected;
	}

	public List<Attribute> getAttributes(String text) {
		List<Attribute> result = new ArrayList<Attribute>();
		ctrl = ScriptUtils.c();
		Node n = ctrl.getSelected();
		if (text.startsWith("ID_")) {
			//Node clicked = ctrl.find(new Node
			Node root = n.getMindMap().getRoot();
			 List<? extends Node> found = root.find(new NodeCondition() {
				
				@Override
				public boolean check(NodeRO n) {
					return n.getId().equals(text);
				}});
			Attributes list = found.get(0).getAttributes();
			for (int i = 0; i < list.size(); i++) {
				Attribute internalAttribute = new Attribute(list.getNames().get(i), list.getNames().get(i));
	    		result.add(internalAttribute);
			}
		}
		else {
			String path = text.substring(text.indexOf("(")+2, text.length()-2);
			try {
				n = n.at(path);
				Attributes list = n.getAttributes();
				for (int i = 0; i < list.size(); i++) {
					Attribute internalAttribute = new Attribute(list.getNames().get(i), list.getNames().get(i));
		    		result.add(internalAttribute);
				}
			} catch (Exception e) {
				System.out.println("Node not found");
			}
		}
		return result;
	}


	private void scrapeNodes(DefaultMutableTreeNode treeNode) {
		MapNode internalNode = (MapNode) treeNode.getUserObject();
		Node containedNode = internalNode.mapNode;
		List<? extends Node> childrenList = containedNode.getChildren();
		for (Node child : childrenList) {
			MapNode InternalNode = new MapNode(child, child.getPlainText(), child.getId());
        	DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(InternalNode);
			setAttributes(newNode);
			scrapeNodes(newNode);
			treeNode.add(newNode);
		}
	}

	/**
	 * Sets the attributes of the tree node containing the map node
	 *
	 * @param treeNode the Node to set
	 */
	private void setAttributes(DefaultMutableTreeNode treeNode) {
		MapNode containedNode = (MapNode) treeNode.getUserObject();
		Attributes attributeList = containedNode.mapNode.getAttributes();
		for (int i = 0; i < attributeList.size(); i++) {
			Attribute internalAttribute = new Attribute(attributeList.getNames().get(i), attributeList.getNames().get(i));
    		treeNode.add(new DefaultMutableTreeNode(internalAttribute));
    	}
	}
}