
package logic;

import java.util.ArrayList;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;


public class MockupMapTree {
		
	public JTree getRoot() {
	    DefaultMutableTreeNode root=new DefaultMutableTreeNode(new NodeMap("Root", "Table", "000"));
	    DefaultMutableTreeNode TablePlan = new DefaultMutableTreeNode(new NodeMap("TablePlan", "Plan", "001"));
	    TablePlan.add(new DefaultMutableTreeNode(new Attribute("Material", "Wood")));
	    TablePlan.add(new DefaultMutableTreeNode(new Attribute("Color", "White")));
	    root.add(TablePlan);
	    DefaultMutableTreeNode leg01 = new DefaultMutableTreeNode(new NodeMap("ALeg", "Leg01", "002"));
	    leg01.add(new DefaultMutableTreeNode(new Attribute("LegMaterial", "Wood")));
	    leg01.add(new DefaultMutableTreeNode(new Attribute("Color", "White")));
	    DefaultMutableTreeNode leg02 = new DefaultMutableTreeNode(new NodeMap("ALeg", "Leg02", "003"));
	    leg02.add(new DefaultMutableTreeNode(new Attribute("LegMaterial", "Wood")));
	    leg02.add(new DefaultMutableTreeNode(new Attribute("Color", "Black")));
	    DefaultMutableTreeNode leg03 = new DefaultMutableTreeNode(new NodeMap("ALeg", "Leg03", "004"));
	    leg03.add(new DefaultMutableTreeNode(new Attribute("LegMaterial", "Metal")));
	    leg03.add(new DefaultMutableTreeNode(new Attribute("Color", "Red")));
	    DefaultMutableTreeNode leg04 = new DefaultMutableTreeNode(new NodeMap("ALeg", "Leg04", "005"));
	    leg04.add(new DefaultMutableTreeNode(new Attribute("LegMaterial", "Metal")));
	    leg04.add(new DefaultMutableTreeNode(new Attribute("Color", "Blue")));
	    TablePlan.add(leg01);
	    TablePlan.add(leg02);
	    TablePlan.add(leg03);
	    TablePlan.add(leg04);
	    return new JTree(root);
	}
	
	
	
	
	//Mockup of a Map, made using a simple tree: 
	//Attribute
	public class Attribute {
		private String name;
		private String value;
		
		public Attribute(String name, String value) {
			super();
			this.name = name;
			this.value = value;
		}

		@Override
		public String toString() {
			return name + ": " + value;
		}
		public String getName() {
			return name;
		}
		public String getValue() {
			return value;
		}
	}
	
	//Node of the map
	public class NodeMap {
		private String Name;
		private String Alias;
		private String Id;
		public ArrayList<Attribute> attributes = new ArrayList<Attribute>();
		
		public NodeMap(String name, String alias, String id) {
			super();
			Name = name;
			Alias = alias;
			Id = id;
		}
		
		public String getName() {
			return Name;
		}

		public String getAlias() {
			return Alias;
		}

		public String getId() {
			return Id;
		}

		public void addAttribute(String _name, String _value) {
			this.attributes.add(new Attribute(_name, _value));
		}
		@Override
		public String toString() {
			return Alias;
		}
	}
}

