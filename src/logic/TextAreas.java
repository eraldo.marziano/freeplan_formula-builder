/*
 * @Author Eraldo Marziano
 */
package logic;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.ImageIcon;
import org.fife.ui.autocomplete.*;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

public class TextAreas {
	private CustomCompletionProvider dotProv;
	private DefaultCompletionProvider noDotProv;
	private FunctionsCompletionProvider customProv;
	/** The XML document, containing all the Template snippets*/
	private Document XMLDocTemplates = null;
	private Document XMLDocMethods = null;
	private RSyntaxTextArea textArea = null;
	/** The list in which the hidden suggestions are stored */
	private List<Completion> removedList;
	AutoCompletion acFunctions;

	
public TextAreas() {
	dotProv = new CustomCompletionProvider();
	customProv = new FunctionsCompletionProvider();
	noDotProv = new DefaultCompletionProvider();
	removedList = new ArrayList<Completion>();
}
/**
 * Hides or shows a category of suggestions. 
 * @param r the relevance of the category
 * @param selected if true shows the category, if false hides it
 */
public void filterFunctionsOut(int r, boolean selected) {
	System.out.println("Filtro");
	List<Completion> list = noDotProv.getCompletions(textArea);
	if (selected) {
		Iterator<Completion> i = removedList.iterator();
		while (i.hasNext()) {
			Completion c = i.next();
			if (c.getRelevance() == r) {
				noDotProv.addCompletion(c);
				i.remove();
			}
		}
	}
	else 
	for (Completion completion : list) {
		if (completion.getRelevance() == r) {
			noDotProv.removeCompletion(completion);
			removedList.add(completion);
		}
	}
}

public void showFunctions(boolean show) {
	if (show) 
		acFunctions.install(textArea);
	else
		acFunctions.uninstall();
}

public RTextScrollPane getTextWindow(Document templates, Document methods) {
	XMLDocTemplates = templates;
	XMLDocMethods = methods;
	dotProv = new CustomCompletionProvider();
	noDotProv = new DefaultCompletionProvider();
    textArea = new RSyntaxTextArea(15, 60);
    textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_GROOVY);
    textArea.setCodeFoldingEnabled(true);
    createAndInstall();
    RTextScrollPane panel = new RTextScrollPane(textArea);
    return panel;
}

private void createAndInstall() {
	noDotProv.setAutoActivationRules(true, null);
	dotProv.setAutoActivationRules(true, "[");
	customProv.setAutoActivationRules(true, null);
	customProv.setParameterizedCompletionParams('(', ", ", ')');
	
	
	
	// creates the Autocompleters
	AutoCompletion acNoDot = new AutoCompletion(noDotProv);
	AutoCompletion acDot = new AutoCompletion(dotProv);
	acFunctions = new AutoCompletion(customProv);
	// sets the Cell Renderer
	CompletionCellRenderer ccr = new CompletionCellRenderer();
	ccr.setShowTypes(true);
	ccr.setTypeColor(new Color(255,0,0));
	acNoDot.setListCellRenderer(ccr);
	acDot.setListCellRenderer(ccr);
	acFunctions.setListCellRenderer(ccr);
	// Sets the Autocompletions
	acNoDot.setAutoActivationDelay(300);
	acNoDot.setAutoActivationEnabled(true);
	acNoDot.setAutoCompleteSingleChoices(false);
	acNoDot.setParameterAssistanceEnabled(true);
	acNoDot.setShowDescWindow(true);
	acDot.setAutoActivationDelay(300);
	acDot.setAutoActivationEnabled(true);
	acDot.setAutoCompleteSingleChoices(false);
	acDot.setParameterAssistanceEnabled(true);
	acFunctions.setShowDescWindow(true);
	acFunctions.setAutoActivationDelay(300);
	acFunctions.setAutoActivationEnabled(true);
	acFunctions.setAutoCompleteSingleChoices(false);
	acFunctions.setParameterAssistanceEnabled(true);
	acFunctions.setShowDescWindow(true);
	loadFromXml(XMLDocTemplates, 100, acNoDot);
	loadFromXml(XMLDocMethods, 90, acNoDot);
	//loadGroups(acFunctions);
	acNoDot.install(textArea);
	acDot.install(textArea);
	acFunctions.install(textArea);
}

private void loadFromXml(Document doc, int relevance, AutoCompletion ac) {
	ImageIcon ico = null;
	//if relevance = 100 -> Template
	if (relevance == 100) {
		ico = new ImageIcon("./resources/Template.png");
		NodeList nList = doc.getElementsByTagName("Template");
		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;
					TemplateCompletion t =	new TemplateCompletion(
							ac.getCompletionProvider(),
							eElement.getElementsByTagName("text").item(0).getTextContent(),
							eElement.getElementsByTagName("tip").item(0).getTextContent(),
							eElement.getElementsByTagName("subs").item(0).getTextContent(),
							null,
							eElement.getElementsByTagName("explanation").item(0).getTextContent());
        			//change Icon and relevance according to different type
        			t.setIcon(ico);
        			t.setRelevance(relevance);
        			((DefaultCompletionProvider) ac.getCompletionProvider()).addCompletion(t);
        	}
    	}
	// if relevance == 90 -> Functions
	} else if (relevance == 90) {
		ico = new ImageIcon("./resources/Method.png");
		NodeList nList = doc.getElementsByTagName("function");
		for (int i = 0; i < nList.getLength(); i++) {
			Node node = nList.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) node;
					FunctionCompletion t = new FunctionCompletion(ac.getCompletionProvider(), 
							eElement.getElementsByTagName("Name").item(0).getTextContent(),
							eElement.getElementsByTagName("tip").item(0).getTextContent());	
							t.setSummary(eElement.getElementsByTagName("Summary").item(0).getTextContent());
							t.setIcon(ico);
							t.setRelevance(relevance);
					((DefaultCompletionProvider) ac.getCompletionProvider()).addCompletion(t); 
        			}
			}
		}
	}
}
