package logic;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.JTextComponent;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.Completion;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

public class CustomCompletionProvider extends DefaultCompletionProvider{
	private FreePlane fp = new FreePlane();

	public CustomCompletionProvider() {
		super();
	}
	public CustomCompletionProvider(String[] words) {
		super(words);
	}

	@Override
	protected List<Completion> getCompletionsImpl(JTextComponent comp) {
		List<Completion> complets = new ArrayList<Completion>();
		List<Completion> temp = super.getCompletionsImpl(comp);
		for (Completion completion : temp) {
			complets.add(completion);
		}
		List<Completion> varToAdd = getAttributesCompletions(comp);
			if (varToAdd!=null) {
			complets.addAll(varToAdd);
		}
		return complets;
	}

	private ArrayList<Completion> getAttributesCompletions(JTextComponent comp) {
		ArrayList<Completion> result = new ArrayList<Completion>();
		RSyntaxTextArea textArea = (RSyntaxTextArea)comp;
		DefaultCompletionProvider p = new DefaultCompletionProvider();
		String temp = textArea.getText();
		String text = searchBeforeActivator(temp);
		if (text != null) {
			System.out.println(text);
			List<Attribute> attributes = fp.getAttributes(text);
			for (Attribute attribute : attributes) {
				System.out.println(attribute.name);
				BasicCompletion b = new BasicCompletion(p, "'" + attribute.name + "']", "Attribute", "<b>Current Value</b>/n" + attribute.value);
				b.setRelevance(130);
				result.add(b);
			}
		}
		return result;
		
	}

	private String searchBeforeActivator(String text) {
		String result = null;
		//checks if ends with dot
		Pattern pattern = Pattern.compile("(node.at\\(\".*\"\\))\\[$");
		Matcher matcher = pattern.matcher(text);
		if(matcher.find()) {
			result = matcher.group(1);
			System.out.println(result);
		}
			
		else {
			pattern = Pattern.compile("(ID_[\\d]*)\\[$");
			matcher = pattern.matcher(text);
			if(matcher.find())
			result = matcher.group(1);
			}
		return result;
	}


}
